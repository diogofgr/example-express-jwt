const express = require('express');
const router = express.Router();
const { getAllUsers } = require('../controllers/users-controller');
const { authenticateWithJwt } = require('../services/jwt');

router.get('/users', authenticateWithJwt, getAllUsers);

module.exports = router;
