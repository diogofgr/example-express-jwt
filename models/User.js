const connection = require('../db/config');
const User = {};

const cleanUser = (user) => {
    return user ?
        {
            ...user,
            passwordHash: 'hidden'
        } :
        undefined;
};
 
 
User.findByEmailAndPassword = (email, password, callback) => {
    connection.query(
        `SELECT * FROM user WHERE email = ? AND passwordHash = SHA2(?, 256)`,
        [email, password],
        (err, results, fields) => callback(err, cleanUser(results[0]), fields)
    )
}

User.getAll = (callback) => {
    connection.query(
        `SELECT id, email FROM user`,
        (err, results, fields) => callback(err, results, fields)
    )
}
 
module.exports = User;
